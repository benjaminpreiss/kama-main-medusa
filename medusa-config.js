const dotenv = require('dotenv')

try {
  dotenv.config({ path: process.cwd() + '/.env' });
 } catch (e) {
  //handle error
 }

const mode = process.env.NODE_ENV
const modePrefix = mode === "production" ? "MEDUSA_STORE_PROD_" : mode === "development" ? "MEDUSA_STORE_DEV_" : ""

// CORS when consuming Medusa from admin
const ADMIN_CORS = process.env[modePrefix + "ADMIN_CORS"] || "http://localhost:7000,http://localhost:7001";

// CORS to avoid issues when consuming Medusa from a client
const STORE_CORS = process.env[modePrefix + "STORE_CORS"] || "http://localhost:8000";

const JWT_SECRET = process.env[modePrefix + "JWT_SECRET"] || "supersecret";

const COOKIE_SECRET = process.env[modePrefix + "COOKIE_SECRET"] || "supersecret";

// Database URL (here we use a local database called medusa-development)
const DATABASE_URL =
  process.env[modePrefix + "DATABASE_URL"] || "postgres://localhost/medusa-store";

// Medusa uses Redis, so this needs configuration as well
const REDIS_URL = process.env.REDIS_URL || "redis://localhost:6379";

// Stripe keys
const STRIPE_API_KEY = process.env.STRIPE_API_KEY || "";
const STRIPE_WEBHOOK_SECRET = process.env.STRIPE_WEBHOOK_SECRET || "";

// This is the place to include plugins. See API documentation for a thorough guide on plugins.
const plugins = [
  `medusa-fulfillment-manual`,
  `medusa-payment-manual`,
  // Uncomment to add Stripe support.
  // You can create a Stripe account via: https://stripe.com
  // {
  //   resolve: `medusa-payment-stripe`,
  //   options: {
  //     api_key: STRIPE_API_KEY,
  //     webhook_secret: STRIPE_WEBHOOK_SECRET,
  //   },
  // },
];

module.exports = {
  projectConfig: {
    // redis_url: REDIS_URL,
    // For more production-like environment install PostgresQL
    // database_database: "./medusa-db.sql",
    // database_type: "sqlite",
    database_url: DATABASE_URL,
    database_type: "postgres",
    store_cors: STORE_CORS,
    admin_cors: ADMIN_CORS,
    jwt_secret: JWT_SECRET,
    cookie_secret: COOKIE_SECRET
  },
  plugins,
};
