#!/bin/bash

dir="$(dirname $(greadlink -f  $0 || readlink -f $0))"
source ${dir}/../source-env.sh

cd $dir
npm run start